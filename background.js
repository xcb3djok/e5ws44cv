
const interval = 1000;

function fn() {
	var array = [];
	var storage = [];
	var reset = false;

	browser.storage.local.get("value").then( (ret) => {
		if (ret.value && (typeof(ret.value) === 'object')) {
			array = ret.value;

			browser.tabs.query({}).then(tabs => {
				for (var b = 0; b < array.length; b++) {
					if (!array[b])
						continue;
					else if (typeof(array[b]) !== 'string') {
						console.log(`Not a string`);
						continue;
					}
					const data = array[b].split(';');
					data[2] = Number(data[2]);
					//console.log(`url ${data[0]} limit ${data[1]} time ${data[2]}`);
					
					const date = new Date().toJSON().slice(0, 10);
					if (data[3] != date) {
						console.log(`Reset time, ${data[3]} <=> ${date}`);
						let str = data[0] + ';' + data[1] + ';' + 0 + ';' + date;
						storage.push(str);
						continue;
					}

					for (var a = 0; a < tabs.length; a++) {
						if (tabs[a].url.includes(data[0])) {
							console.log(`url ${tabs[a].url} includes ${data[0]}`);
							data[2] = data[2] + (interval / 1000);
							//console.log(`time increment ${data[2]}`);
							if (data[2] >= data[1]) { // Notification every minute
								if ((data[2] % 60) == 0) {
									console.log(`time exceeded ${data[2]} > ${data[1]}`);
									const str = `Timer expired for ${data[0]}`;
									chrome.notifications.create({
										"type": "basic", "title": "Site timer",
										"message": str, "iconUrl": "icon.svg"});
									const audio = new Audio("sound.mp3");
									audio.play();
								}
							}
							let str = data[0] + ';' + data[1] + ';' +
								data[2] + ';' + data[3];
							array[b] = str;
						}
					}
					storage.push(array[b]);
				}
				if (storage.length > 0) {
					let pair = {value: storage};
					browser.storage.local.set(pair).then(
						(ret) => {console.log(`Added ${storage} to storage`);},
						(ret) => {console.error("Error adding to storage");});
				}
			}, (ret) => {
				console.warn("Could not get tabs"); 
			});
		}
	}, (ret) => {
		console.warn("no data");
	});
}
window.setInterval(fn, interval);

function handleStorage(ret, request) {
	var url = request[0];
	var time = request[1];
	var array = [];
	var storage = [];
	var update = false;

	let isnum = /^\d+$/.test(time);

	if (url == undefined || time == undefined) {
		console.warn(`empty url ${url} or time ${time}`);
		return;
	}
	else if (!isnum) {
		console.warn(`Time ${time} is not a number`);
		return;
	}
	else if (typeof(url) !== 'string') {
		console.warn(`Url ${url} is not a string`);
		return;
	}
	time = time * 60; // deal with seconds
	const date = new Date().toJSON().slice(0, 10);
	const str = url + ';' + time + ';' + 0 + ';' + date;

	if (ret.value) {
		if (ret.value.length < 1) {
			console.log(`Storage empty`);
			storage.push(str);
		}	
		else {	
			console.log(`Storage exists`);
			array = ret.value;

			for (var a = 0; a < array.length; a++) {
				if (!array[a])
					continue;
				else if (typeof(array[a]) !== 'string') {
					console.log(`Not a string`);
					continue;
				}
				console.log(`Entry index ${a} found ${array[a]}`);
				const data = array[a].split(';');

				if (data[0] == url) {
					if (time < 1) { // Remove URL
						console.log(`Time ${time} < 1 => removed`);
					} else {
						console.log(`Update time from ${data[1]} to ${time}`);
						const str = data[0] + ';' + time + ';' + data[2];
						storage.push(str);
					}
					update = true;
				}
				else if (data[1] > 0)
					storage.push(array[a]);
			}
			if (!update) {
				console.log(`New entry ${str}`);
				storage = [str].concat(storage);
			}
		}
	}
	else {
		console.log(`Storage does not exist`);
		storage.push(str);
	}
	let pair = {value: storage};
	browser.storage.local.set(pair).then(
		(ret) => {console.log(`Added ${storage} to storage`);},
		(ret) => {console.error("Error adding to storage");});
}

function handleMessage(request, sender, sendResponse) {
	console.log(`Content script sent a message: ${request}`);

	browser.storage.local.get("value").then(
		(ret) => { handleStorage(ret, request) },
		(ret) => { console.warn("no data"); }
	);

	sendResponse(request);
}

browser.runtime.onMessage.addListener(handleMessage);
