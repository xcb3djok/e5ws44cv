# Site timer
Web extension which lets you set timer limiters for sites.
Upon timer expiration extension will send notifications and play sound.

## Design
- Stick to native Web
- Conform to Chrome and Firefox natively
- Notifications and audio will be sent every minute after expiration

## Licenses
icon.svg [Apache-2.0]
sound.mp3 [Mixkit Sound Effects Free License]
