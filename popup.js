
var update = false;

function each(value, index, array) {
	if (!value) return;
	if (typeof(value) !== 'string') return;

	const data = value.split(';');
	console.log(`value ${value} split ${data} 
		index ${index} array ${array} length ${array.length}`);
	data[1] = Number(data[1]);
	data[1] = data[1] / 60;
	console.log(`update ${update}`);

	var element = document.createElement("div");
	element.id = data[0];

	var paragraph = document.createElement("label");
	var textnode = document.createTextNode(`${data[0]} ${data[1]}`);
	paragraph.appendChild(textnode);
	element.appendChild(paragraph);
	
	var button = document.createElement("button");
	button.innerText = "Remove";
	button.onclick = remove;
	element.appendChild(button);

	var targetElement = document.getElementById("timers");
	targetElement.appendChild(element);
}

function load() {
	browser.storage.local.get("value").then(
		(ret) => {
			if (ret.value && (typeof(ret.value) === 'object')) {
				if (update) {
					var targetElement = document.getElementById("timers");
					targetElement.replaceChildren();
					ret.value.forEach(each);
					update = false;
				}
			}
		},
		(ret) => {
			console.warn("no data");
		}
	);
}

function backgroundResponse(args) {
	console.log(`Background response ${args}`);
	update = true;
}

browser.storage.onChanged.addListener(load);

function remove(elem) {
	this.parentNode.remove();
	chrome.runtime.sendMessage([this.parentNode.id, 0], backgroundResponse);
}

document.getElementById("button").addEventListener("click", () => {
	update = true;
	load();
});

document.addEventListener("DOMContentLoaded", function() {
	update = true;
	load();
});

function handleError(error) {
	console.log(`Error: ${error}`);
}

document.getElementById("button").addEventListener("click", () => {
	let url = document.getElementById("site").value;
	let time = document.getElementById("time").value;
	chrome.runtime.sendMessage([url, time], backgroundResponse);
});

